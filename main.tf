variable region {}                               
variable instance_type {}
variable zone_id {}
variable api_key {}
variable email {}

#Добавим вот такую запись.  Без нее не работает.
terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "3.33.1"
    }
  }
}
provider "cloudflare" {
 api_key = var.api_key
 email = var.email
}


provider "aws" {
  region     = var.region
}

# Запросим ID VPC
data "aws_vpc" "default" {
  default = true
}

# делаем инстанс
resource "aws_instance" "ec2" {                 
  ami                         = "ami-00874d747dde814fa"
  instance_type               = var.instance_type
  key_name                    = "gitlab-runner"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.tf53-sg.id]

 tags = {
    Name = "tf5_3"
  }

# Делаем 2 диска. Один будет Root на 10 гб, второй просто примапленный EBS  на 8 гб. 

  root_block_device {
    volume_size = 10
    volume_type = "gp2"
  }
    
    ebs_block_device {
     device_name = "/dev/sdc"
    volume_size = 8
    volume_type = "gp2"
  }

# Запускаем скрипт по  установке Докера, нжинкса. 

user_data = file("./userdata1.sh")

}
# Делаем группу секурити ,  VPC остпавляем default

# Создадим Security group


resource "aws_security_group" "tf53-sg" { 
  name   = "tf53-sg"
  vpc_id = data.aws_vpc.default.id


  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8001
    to_port     = 8001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 8802
    to_port     = 8802
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

} 

# Узнаем Public IP нашей ЕС2

output "public_ip" {
 value = aws_instance.ec2.public_ip
}

#Теперь идем крутить клауд ДНС

resource "cloudflare_record" "juneway" {
 zone_id = var.zone_id
 name = "fstep.juneway.pro"
 value = aws_instance.ec2.public_ip
 type = "A"
 ttl = "3600"
}

